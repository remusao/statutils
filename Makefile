# Compilation options
CXX=g++
CXXFLAGS=-std=c++11 -Wall -Wextra -pedantic -fPIC -c
LDFLAGS=-Iinclude -Isrc -I/usr/include/eigen3

SRC=

# Output file options
LIB=lib/libstatutils
PREFIX=/usr/local/lib

all: statutils

statutils:
	@test -d lib || mkdir lib
	${CXX} ${CXXFLAGS} ${SRC} ${LDFLAGS}
	${CXX} -shared -o ${LIB}.so $(shell ls *.o)

install: all
	@test -d $(PREFIX) || mkdir -p $(PREFIX)
	@install -m 0755 ${LIB}.so ${PREFIX}


clean:
	@rm -frv lib
