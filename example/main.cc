#include <iostream>
#include <string>

#include <basic_types.hh>
#include <document_statistics.hh>
#include <metrics.hh>

int main()
{
    using Word = stat::Word<std::string>;
    using Document = stat::Document<Word>;
    using Corpus = stat::Corpus<Document>;

    // Load corpus
    auto corpus = stat::CorpusFactory<Document>::from_path("./corpus/");

    // Build TermDocumentMatrix
    auto term_document_matrix = stat::compute_tf(corpus);

    std::cout << corpus.size() << std::endl;
    std::cout << Corpus::document_type::getNbTerms() << std::endl;
    std::cout << term_document_matrix << std::endl;

    return 0;
}
