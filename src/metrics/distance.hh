#ifndef DISTANCES_HH_
# define DISTANCES_HH_

# include <armadillo>

namespace stat
{
    double euclidean_distance(const arma::mat& a, const arma::mat& b);

    double euclidean_distance_squared(const arma::mat& a, const arma::mat& b);

    double cosine_distance(const arma::mat& a, const arma::mat& b);
}

// Implementations
# include "distance.hxx"

#endif /* DISTANCES_HH_ */
