
namespace stat
{
    double euclidean_distance(const arma::mat& a, const arma::mat& b)
    {
        return arma::norm(a - b, 2);
    }

    double euclidean_distance_squared(const arma::mat& a, const arma::mat& b)
    {
        return arma::norm(a - b, 1);
    }

    double cosine_distance(const arma::mat& a, const arma::mat& b)
    {
        return arma::dot(a, b) / (arma::norm(a, 2) * arma::norm(b, 2));
    }
}
