#ifndef DOCUMENT_HH_
# define DOCUMENT_HH_

# include <vector>
# include "basic_document.hh"

namespace stat
{
    template <typename W>
    using Document = BasicDocument<W, std::vector>;
}

#endif /* !DOCUMENT_HH_ */
