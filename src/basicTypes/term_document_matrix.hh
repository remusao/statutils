#ifndef TERM_DOCUMENT_MATRIX_HH_
# define TERM_DOCUMENT_MATRIX_HH_

# include <armadillo>

namespace stat
{
    using TermDocumentMatrix = arma::mat;
}

#endif /* !TERM_DOCUMENT_MATRIX_HH_ */
