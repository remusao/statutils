#ifndef BAG_OF_WORDS_HH_
# define BAG_OF_WORDS_HH_

# include <set>
# include "basic_document.hh"
# include "document.hh"

namespace stat
{
    template <typename W>
    using BagOfWords = BasicDocument<W, std::set>;
}

#endif /* !BAG_OF_WORDS_HH_ */
