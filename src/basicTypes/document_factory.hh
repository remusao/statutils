#ifndef DOCUMENT_FACTORY_HH_
# define DOCUMENT_FACTORY_HH_

# include <string>
# include <fstream>

namespace stat
{
    template <typename Document>
    class DocumentFactory
    {
        public:
            /// Create a document from a file
            static Document
            from_file(const char* f)
            {
                return from_file(std::string(f));
            }


            static Document
            from_file(const std::string& f)
            {
                std::ifstream ifs(f, std::ifstream::in);
                Document document(f);
                typename Document::word_internal_type word;

                while (ifs.good())
                {
                    ifs >> word;
                    document.addWord(word);
                }

                return document;
            }


            static Document
            from_db(const char* /* f */, const char* /* name */)
            {
                return Document();
            }


            static Document
            from_remote(const char* /* remote */, const char* /* name */)
            {
                return Document();
            }
    };
}

#endif /* !DOCUMENT_FACTORY_HH_ */
