#ifndef WORD_HH_
# define WORD_HH_

# include <utility>

namespace stat
{
    template <typename S>
    class Word
    {
        public:
            // Types
            typedef S word_internal_type;

            // Constructors
            ~Word() = default;
            Word() = delete;

            Word(S&& w)
                : w_(std::forward<S>(w))
            {
            }

            // Copy
            Word(const Word&) = delete;
            Word& operator=(const Word&) = delete;

            // Move
            Word(Word&&) = default;
            Word& operator=(Word&&) = default;

            const S& getWord() const
            {
                return w_;
            }

        private:
            S   w_;
    };
}

#endif /* !WORD_HH_ */
