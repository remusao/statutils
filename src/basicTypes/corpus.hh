#ifndef CORPUS_HH_
# define CORPUS_HH_

# include <vector>

namespace stat
{
    template <typename D>
    class Corpus
    {
        private:
            typedef std::vector<D>                  doc_container_;

        public:
            // Types
            typedef typename D::id_type             id_type;
            typedef typename D::word_internal_type  word_internal_type;
            typedef typename D::word_type           word_type;
            typedef D                               document_type;


            // Constructors
            ~Corpus() = default;

            // Copy
            Corpus(const Corpus&) = delete;
            Corpus& operator=(const Corpus&) = delete;

            // Move
            Corpus(Corpus&&) = default;
            Corpus& operator=(Corpus&&) = default;

            // Methods
            int size() const
            {
                return documents_.size();
            }

            typename doc_container_::iterator begin()
            {
                return documents_.begin();
            }

            typename doc_container_::const_iterator begin() const
            {
                return documents_.cbegin();
            }

            typename doc_container_::iterator end()
            {
                return documents_.end();
            }

            typename doc_container_::const_iterator end() const
            {
                return documents_.cend();
            }

            // Static methods

            static const std::string& getUrl(id_type id)
            {
                return Corpus::id2doc_[id];
            }

            static id_type getId(const std::string& doc)
            {
                return Corpus::doc2id_[doc];
            }


        private:
            // Only created by a factory
            Corpus() = default;

            // Add a document to the Corpus
            void addDocument(D&& document)
            {
                const auto& url = document.getUrl();
                auto it = Corpus::doc2id_.find(url);

                if (it == Corpus::doc2id_.end())
                {
                    Corpus::doc2id_.emplace(url, Corpus::nextId_);
                    Corpus::id2doc_.push_back(url);
                    ++Corpus::nextId_;
                }
                documents_.emplace_back(std::move(document));
            }

            // Static attributes
            static std::vector<std::string>                  id2doc_;
            static std::unordered_map<std::string, id_type>  doc2id_;
            static id_type                                   nextId_;

            template <typename Document>
            friend class CorpusFactory;

            doc_container_ documents_;
    };

    // Declare static members of Corpus

    template <typename D>
    typename std::vector<std::string> Corpus<D>::id2doc_;

    template <typename D>
    typename std::unordered_map<std::string, typename Corpus<D>::id_type> Corpus<D>::doc2id_;

    template <typename D>
    typename Corpus<D>::id_type Corpus<D>::nextId_{0};
}

#endif /* !CORPUS_HH_ */
