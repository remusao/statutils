#ifndef BASIC_DOCUMENT_HH_
# define BASIC_DOCUMENT_HH_

# include <unordered_map>
# include <string>

namespace stat
{
    template <typename W, template <typename...> class T>
    class BasicDocument
    {
        public:
            // Types
            typedef uint64_t                        id_type;
            typedef typename W::word_internal_type  word_internal_type;
            typedef W                               word_type;
            typedef T<id_type>                      container_type;

            ~BasicDocument() = default;

            template <typename Iterator>
            BasicDocument(const char* url, const Iterator& b, const Iterator& e)
                : document_(b, e),
                  url_(url)
            {
            }

            // Copy
            BasicDocument(const BasicDocument&) = delete;
            BasicDocument& operator=(const BasicDocument&) = delete;

            // Move
            BasicDocument(BasicDocument&&) = default;
            BasicDocument& operator=(BasicDocument&&) = default;


            // Methods
            typename container_type::iterator begin()
            {
                return document_.begin();
            }

            typename container_type::const_iterator begin() const
            {
                return document_.cbegin();
            }

            typename container_type::iterator end()
            {
                return document_.end();
            }

            typename container_type::const_iterator end() const
            {
                return document_.cend();
            }

            const std::string& getUrl() const
            {
                return url_;
            }

            uint64_t size() const
            {
                return document_.size();
            }

            //
            // Static methods
            //

            static const word_internal_type& getWord(id_type id)
            {
                return BasicDocument::id2word_[id];
            }

            static id_type getId(const W& word)
            {
                return BasicDocument::word2id_[word];
            }

            static id_type getNbTerms()
            {
                return BasicDocument::id2word_.size();
            }

        protected:

            template <typename Document>
            friend class DocumentFactory;

            // Add a word to the document
            void addWord(const word_internal_type& word)
            {
                auto it = word2id_.find(word);

                if (it == word2id_.end())
                {
                    word2id_.emplace(word, nextId_);
                    id2word_.push_back(word);
                    document_.insert(document_.end(), nextId_);
                    ++nextId_;
                }
                else
                {
                    document_.insert(document_.end(), it->second);
                }
            }

            // Constructors - Only created by factory
            BasicDocument() = default;
            BasicDocument(const std::string& name)
                : url_(name)
            {
            }
            BasicDocument(const char* name)
                : url_(name)
            {
            }

            // Static
            static std::vector<word_internal_type>                  id2word_;
            static std::unordered_map<word_internal_type, id_type>  word2id_;
            static id_type                                          nextId_;

            // Attributes
            container_type  document_;
            std::string     url_;
    };

    // Declare static members of basic_document

    template <typename W, template <typename...> class T>
    typename std::vector<typename BasicDocument<W, T>::word_internal_type> BasicDocument<W, T>::id2word_;

    template <typename W, template <typename...> class T>
    typename std::unordered_map<typename BasicDocument<W, T>::word_internal_type, typename BasicDocument<W, T>::id_type> BasicDocument<W, T>::word2id_;

    template <typename W, template <typename...> class T>
    typename BasicDocument<W, T>::id_type BasicDocument<W, T>::nextId_{0};
}

#endif /* !BASIC_DOCUMENT_HH_ */
