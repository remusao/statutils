#ifndef STAT_TRAITS_HH_
# define STAT_TRAITS_HH_

# include <string>
# include "document.hh"
# include "bag_of_words.hh"
# include "corpus.hh"

namespace stat
{
    template <typename W>
    const std::string& getUrl(const Document<W>& d)
    {
        return d.getUrl();
    }

    template <typename W>
    const std::string& getUrl(const BagOfWords<W>& d)
    {
        return d.getUrl();
    }
}

#endif /* STAT_TRAITS_HH_ */
