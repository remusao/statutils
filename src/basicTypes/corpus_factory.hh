#ifndef CORPUS_FACTORY_HH_
# define CORPUS_FACTORY_HH_

# include <sys/types.h>
# include <dirent.h>

# include <iostream>
# include <string>

# include "document_factory.hh"

namespace stat
{
    template <typename D>
    class CorpusFactory
    {
        public:

            static Corpus<D>
            from_path(const char* p)
            {
                return from_path(std::string(p));
            }

            static Corpus<D>
            from_path(const std::string& p)
            {
                Corpus<D> corpus;
                DIR *dir = NULL;
                struct dirent *ent = NULL;

                // Open directory file
                if ((dir = opendir(p.c_str())) != NULL)
                {
                    // For each regular file in the directory, create a Document
                    while ((ent = readdir(dir)) != NULL)
                    {
                        if (ent->d_type == DT_REG)
                        {
                            // Add the document in the corpus
                            std::string path{p};
                            path.append(ent->d_name);
                            corpus.addDocument(DocumentFactory<D>::from_file(path));
                        }
                    }
                    closedir(dir);
                }
                else
                {
                    std::cerr << "Unable to open directory: " << p << std::endl;
                }

                return corpus;
            }


            static Corpus<D>
            from_local(const char* /* db */)
            {
                return Corpus<D>();
            }


            static Corpus<D>
            from_remote(const char* /* remote */)
            {
                return Corpus<D>();
            }
    };
}

#endif /* !CORPUS_FACTORY_HH_ */
