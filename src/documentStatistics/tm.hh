#ifndef TM_HH_
# define TM_HH_

# include <basic_types.hh>

namespace stat
{
    template <typename C>
    TermDocumentMatrix compute_terms(const C& corpus);
}

// Implementation
# include "tm.hxx"

#endif /* !TM_HH_ */
