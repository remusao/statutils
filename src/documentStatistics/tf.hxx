
namespace stat
{
    template <typename C>
    TermDocumentMatrix compute_tf(const C& corpus)
    {
        TermDocumentMatrix result{arma::zeros(C::document_type::getNbTerms(), corpus.size())};
        int doc_id = 0;

        // Compute term frequency
        for (const auto& document: corpus)
        {
            for (const auto& word_id: document)
            {
                ++result(word_id, doc_id);
            }

            result.col(doc_id) /= (float)document.size();

            ++doc_id;
        }


        return result;
    }
}
