#ifndef TF_HH_
# define TF_HH_

# include <basic_types.hh>

namespace stat
{
    template <typename C>
    TermDocumentMatrix compute_tf(const C& corpus);
}

// Implementation
# include "tf.hxx"

#endif /* !TF_HH_ */
